# Spawncli

Spawncli lets you run several copies of a command-line in parallel, capturing their stdout and stderr.

## Installation

[![Gem Version](https://badge.fury.io/rb/spawncli.svg)](https://badge.fury.io/rb/spawncli)

Spawncli doesn't expose any worthwhile functionality as a library.  It can be installed as a command-line gem from RubyGems:

``gem install spawncli``

Spawncli is available on [RubyGems](https://rubygems.org/gems/spawncli/)

## Usage

``spawncli n command-line arg1 arg2 argN``

Spawncli takes two arguments:  a number and a command to run.  Any further arguments are interpreted as being part of the second.

To avoid shell interpretation of pipes and redirections, the command-line to run should be quoted.

### Example

``spawncli 10 "curl http://localhost:8080/foo"``

### Return Code

Returns 1 if anything spawned process wrote to its stderr, 0 otherwise.
