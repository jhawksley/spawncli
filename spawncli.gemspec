# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)

require 'version'

Gem::Specification.new do |spec|
  spec.name          = "spawncli"
  spec.version       = Spawncli::VERSION
  spec.authors       = ["John Hawksley"]
  spec.email         = ["john.hawksley@gmail.com"]

  spec.summary       = %q{Run several command-lines in parallel.}
  spec.description   = %q{Spawncli forks several processes to run concurrently, and monitors their status.}
  spec.homepage      = "https://github.com/jhawksley/spawncli/"

  spec.files         = `git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(test|spec|features)/}) }
  spec.bindir        = "bin"
  spec.executables   = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]

  spec.add_runtime_dependency "open4", "~> 1.3"

  spec.add_development_dependency "bundler", "~> 1.9"
  spec.add_development_dependency "rake", "~> 10.0"
end
