module Spawncli


  # A class to hold information about a spawned thread.
  class ThreadInfo

    attr_accessor :thread, :id, :stdout, :stderr, :exit_code
    attr_reader :start_time

    # Create a new ThreadInfo holder.
    #   @param id [String] the thread ID
    def initialize(id)
      @id = id
      @start_time = Time.now
    end

  end

end