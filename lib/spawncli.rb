require 'open4'
require 'threadinfo'

module Spawncli

  class Spawncli

    @@help_text = <<-EOF.gsub /^  /, ""
    Spawn:  Spawns 'n' copies of a command line.
    John Hawksley <john.hawksley@gmail.com> -*- MIT License

    See https://github.com/jhawksley/spawn for details.

    Options:
        spawncli 2 "sleep 1 && curl http://localhost"  # Run 2 copies of the command
    EOF


    def run(args)

      # Proces out the arguments - this is a very simple parse since we don't support any options.
      n = process_args(args)

      # Parse out the remainder of the args values as a command line to run in the shell.
      argline = generate_arg_line(args) # Gets rid of the trailing space.

      # An array to hold our ThreadInfo objects.
      threads = spawn_threads(argline, n)

      puts

      # Poll the thread array, seeing if any have completed (status false)
      # or there was an exception (nil).
      # Any exceptions are not currently handled.
      any_stderr = poll_threads(threads)

      # If any thread wrote to stderr, exit 1, otherwise 0.
      exit any_stderr ? 1 : 0

    end

    private

    def poll_threads(threads)
      any_stderr = false

      while threads.length > 0 do
        threads.each do |ti|
          if (ti.thread.status == false || ti.thread.status == nil) then
            # Done or exited with exception
            threads.delete(ti)

            print "#{ti.id}: complete #{ti.thread.status == nil ? '(failed) ' : ''}"
            $stdout.flush
            puts (Time.now - ti.start_time).to_s << 's'

            if ti.stdout != nil && !ti.stdout.empty?
              ti.stdout.each_line do |l|
                puts "#{ti.id}: out:   #{l}"
              end
            end
            if ti.stderr != nil && !ti.stderr.empty?
              any_stderr = true
              ti.stderr.each_line do |l|
                puts "#{ti.id}: err:   #{l}"
              end
            end

            puts
          end
        end
      end
      any_stderr
    end

    def spawn_threads(argline, n)
      threads = Array.new #of ThreadInfos

      puts "Spawning #{n} copies of #{argline}"

      # Do all the spawns
      (1..n).each do |i|
        ti = ThreadInfo.new i #... initializes the internal ID with 'i'
        threads << ti
        thread = Thread.new do
          status = Open4::popen4(argline) do |pid, stdin, stdout, stderr|
            ti.stdout = stdout.read.strip
            ti.stderr = stderr.read.strip
          end
        end
        ti.thread = thread
        puts "#{i}: spawned."
      end
      threads
    end

    def generate_arg_line(args)
      argline=''

      args.each do |a|
        argline = argline + a + ' '
      end

      argline.strip!
      argline
    end

    def process_args(args)
      if args.length < 2 then
        do_help
        abort
      else
        # The first argument is interpreted as the number of concurrent thread
        # to run.
        begin
          n = Integer(args.shift)
        rescue Exception => e
          abort "The first argument of a multiple argument invocation must be an integer (#{e})"
        end
      end
      n
    end


    def do_help
      puts @@help_text
    end

  end

end
